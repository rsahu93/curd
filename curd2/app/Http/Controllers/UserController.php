<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        $users= User::get();
        // dd($users);
        return view('users.index',compact('users'));
    }


    public function create()
    {
        return view('users.create');
    }


    public function store(Request $request)
    {
        $user= new User();
        $user->name =$request->name;
        $user->email =$request->email;
        $user->gender =$request->gender;
        $user->class =$request->class;
        $user->status =$request->status;
        $user->save();
       return redirect('/');
        // dd($request->all());
    }

    public function show($id)
    {
        // dd($id);
        $user=User::find($id);
        return view('users.show',compact('user'));
    }

    public function edit($id)
    {
        $user=User::find($id);
        return view('users.edit',compact('user'));
    }


    public function update(Request $request, $id)
    {
        $user=User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->class = $request->class;
        $user->status = $request->status;
        $user->update();
        return redirect('/');
    }


    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();
        return redirect('/');
        // $user =User::where('id',$id)->get();
        // dd($user);
    }
}

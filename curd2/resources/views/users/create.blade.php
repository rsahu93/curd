<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-light">
        <ul class="navbar-nav">
          <li class="nav-item">
          </li>
          <li class="nav-item">
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"></a>
          </li>
        </ul>
      </nav>

<div class="container">
    <form action="{{ route('store') }}" method="POST">
        @csrf
        <div class="form-group">
          <label for="name">Name :</label>
          <input type="text" class="form-control" placeholder="Enter name" name="name" required>
        </div>
        <div class="form-group">
          <label for="email">Email :</label>
          <input type="email" class="form-control" placeholder="Enter email" name="email" required>
        </div>
        
        <div class="form-group">
            <label for="gender">Gender</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="gender"  value="male" checked>
                <label class="form-check-label" for="male">
                male
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="gender" value="female">
                <label class="form-check-label" for="female">
                  female
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="gender"  value="other">
                <label class="form-check-label" for="other">
                  other
                </label>
              </div>
        </div>

        <div class="form-group">
            <select class="custom-select" name="class">
              <option selected>Class</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
            </select>
        </div>
        <div class="form-group">
            <select class="custom-select" name="status">
              <option selected>Status</option>
              <option value="1">Active</option>
              <option value="2">In Active</option>
            </select>
        </div>
            <button type="submit" class="btn btn-success">Save</button>
            <a href="/"><button type="button" class="btn btn-warning">Cencel</button></a>
      </form>
</body>
</html>
